﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using ReportLib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace UserLoginsReport
{
    class Program
    {
        static void Main(string[] args)
        {
            var logFile = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log.txt"), true);
            var companyTypes = new DataTable();
            var companyTypesLoginHistories = new List<DataTable>();
            var allLoginHistories = new DataTable();
            try
            {
                logFile.WriteLine();
                logFile.WriteLine("===== START {0} =====", DateTime.Now);

                // Get data
                #region Get data

                var connectionString = ConfigurationManager.ConnectionStrings["WebEd9_Fuchs"].ConnectionString;
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    logFile.WriteLine("Connected to Database");

                    var command = new SqlCommand("SELECT Id, Name FROM CompanyTypes WHERE RecordDeleted = 0 ORDER BY Name", conn);
                    companyTypes.Load(command.ExecuteReader());

                    command =
                        new SqlCommand(
                            "SELECT CompanyTypes.Name AS 'COMPANY TYPE', Contacts.Company AS 'COMPANY', Contacts.Title AS 'JOB TITLE', Contacts.FirstName AS 'FIRST NAME', Contacts.LastName AS 'LAST NAME', Users.Email AS 'USER EMAIL', ULH.NumberOfLogins AS 'NUMBER OF LOGINS', ULH.LastLogin AS 'LAST LOGIN DATE', Users.AccountExpiresDate AS 'DEACTIVATION DATE'" +
                            "FROM (SELECT UserId, COUNT(UserId) AS NumberOfLogins, MAX(CREATED) AS LastLogin FROM UserLoginHistories WHERE Created > DATEADD(MONTH, -1, GETUTCDATE()) GROUP BY UserId) AS ULH " +
                            "RIGHT JOIN Users ON ULH.UserId = Users.Id LEFT JOIN Contacts ON Contacts.UserId = Users.Id " +
                            "LEFT JOIN Companies ON Companies.Name = Contacts.Company COLLATE Latin1_General_CS_AS " +
                            "LEFT JOIN CompanyTypes ON CompanyTypes.Id = Companies.CompanyTypeId " +
                            "WHERE Users.RecordDeleted = 0 AND (CompanyTypes.Id = @CompanyTypeId OR @GetAll = 1)", conn);
                    command.Parameters.Add("@CompanyTypeId", SqlDbType.Int);
                    command.Parameters.Add("@GetAll", SqlDbType.Int);

                    // Get all login histories
                    command.Parameters["@GetAll"].Value = 1;
                    command.Parameters["@CompanyTypeId"].Value = 0;
                    allLoginHistories.Load(command.ExecuteReader());

                    // Get login histories for individual company type
                    for (var index = 0; index < companyTypes.Rows.Count; index++)
                    {
                        var companyTypeId = Convert.ToInt32(companyTypes.Rows[index][0].ToString());
                        command.Parameters["@CompanyTypeId"].Value = companyTypeId;
                        command.Parameters["@GetAll"].Value = 0;
                        var companyTypeLoginHistories = new DataTable();
                        companyTypeLoginHistories.Load(command.ExecuteReader());
                        if (companyTypeLoginHistories.Rows.Count > 0)
                        {
                            companyTypeLoginHistories.TableName = companyTypes.Rows[index][1].ToString();
                            companyTypesLoginHistories.Add(companyTypeLoginHistories);
                        }
                    }

                    conn.Close();
                    conn.Dispose();
                    logFile.WriteLine("Close connection to Database");
                }

                #endregion

                // Export data to excel file
                #region Export Data

                const string templateFile = "Template.xlsx";
                var sourcePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, templateFile);

                var targetFileName = ConfigurationManager.AppSettings["TargetFileName"];
                targetFileName = string.Format("{0}_{1}.xlsx", targetFileName, DateTime.Now.ToString("ddMMMyyyy"));
                var reportDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Reports");
                var targetPath = Path.Combine(reportDirectory, targetFileName);

                if (!Directory.Exists(reportDirectory))
                {
                    Directory.CreateDirectory(reportDirectory);
                }

                if (File.Exists(sourcePath))
                {
                    File.Copy(sourcePath, targetPath, true);

                    var dateFormat = ConfigurationManager.AppSettings["DateFormat"];

                    using (var workbook = SpreadsheetDocument.Open(targetPath, true))
                    {
                        var workbookPart = workbook.WorkbookPart;

                        var firstSheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault();

                        if (firstSheet == null) return;

                        workbookPart.Workbook.Descendants<Sheet>().ToList().Add(firstSheet);

                        #region Import data for all company types

                        var worksheetPart = (WorksheetPart)workbookPart.GetPartById(firstSheet.Id);
                        var worksheet = worksheetPart.Worksheet;

                        var sheetData = worksheet.GetFirstChild<SheetData>();

                        var headerRow = new Row();

                        var columns = new List<string>();

                        // Replace header
                        foreach (DataColumn column in allLoginHistories.Columns)
                        {
                            columns.Add(column.ColumnName);

                            var cell = new Cell
                            {
                                DataType = CellValues.String,
                                CellValue = new CellValue(column.ColumnName),
                                StyleIndex = 1
                            };
                            headerRow.AppendChild(cell);
                        }
                        sheetData.ReplaceChild(headerRow, sheetData.FirstChild);

                        // Import data
                        foreach (DataRow loginHistory in allLoginHistories.Rows)
                        {
                            var newRow = new Row();
                            foreach (var col in columns)
                            {
                                var cell = new Cell
                                {
                                    DataType = col.Equals("NUMBER OF LOGINS")
                                                ? CellValues.Number
                                                : CellValues.String,
                                    CellValue = (col.Equals("DEACTIVATION DATE") || col.Equals("LAST LOGIN DATE")) && !string.IsNullOrEmpty(loginHistory[col].ToString())
                                            ? new CellValue(Convert.ToDateTime(loginHistory[col].ToString()).ToString(dateFormat))
                                            : new CellValue(loginHistory[col].ToString()),
                                    StyleIndex = 2
                                };
                                newRow.AppendChild(cell);
                            }

                            sheetData.AppendChild(newRow);
                        }

                        #endregion

                        #region Create company type sheets and import data

                        foreach (var companyTypeLoginHistories in companyTypesLoginHistories)
                        {
                            var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                            sheetData = new SheetData();
                            sheetPart.Worksheet = new Worksheet();

                            var xlCols = GetFormatColumns();

                            sheetPart.Worksheet.Append(xlCols);
                            sheetPart.Worksheet.Append(sheetData);

                            var sheets = workbook.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                            var relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                            uint sheetId = 1;
                            if (sheets.Elements<Sheet>().Any())
                            {
                                sheetId =
                                    sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                            }

                            var sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = companyTypeLoginHistories.TableName };
                            sheets.Append(sheet);

                            headerRow = new Row();

                            columns = new List<string>();
                            foreach (DataColumn column in companyTypeLoginHistories.Columns)
                            {
                                if (column.ColumnName.Equals("COMPANY TYPE"))
                                {
                                    continue;
                                }
                                columns.Add(column.ColumnName);

                                var cell = new Cell
                                {
                                    DataType = CellValues.String,
                                    CellValue = new CellValue(column.ColumnName),
                                    StyleIndex = 1
                                };
                                headerRow.AppendChild(cell);
                            }

                            sheetData.AppendChild(headerRow);

                            foreach (DataRow dsrow in companyTypeLoginHistories.Rows)
                            {
                                var newRow = new Row();
                                foreach (var col in columns)
                                {
                                    var cell = new Cell
                                    {
                                        DataType =
                                            col.Equals("NUMBER OF LOGINS")
                                                ? CellValues.Number
                                                : CellValues.String,
                                        CellValue = (col.Equals("DEACTIVATION DATE") || col.Equals("LAST LOGIN DATE")) && !string.IsNullOrEmpty(dsrow[col].ToString())
                                            ? new CellValue(Convert.ToDateTime(dsrow[col].ToString()).ToString(dateFormat))
                                            : new CellValue(dsrow[col].ToString()),
                                        StyleIndex = 2
                                    };
                                    newRow.AppendChild(cell);
                                }

                                sheetData.AppendChild(newRow);
                            }
                        }
                        #endregion
                    }

                    // Send mail
                    var emailModel = new EmailModel
                    {
                        From = ConfigurationManager.AppSettings["EmailFrom"],
                        FromName = ConfigurationManager.AppSettings["EmailFromName"],
                        To = ConfigurationManager.AppSettings["EmailTo"],
                        CC = ConfigurationManager.AppSettings["EmailCC"],
                        BCC = ConfigurationManager.AppSettings["EmailBCC"],
                        Subject = ConfigurationManager.AppSettings["EmailSubject"],
                        Body = ConfigurationManager.AppSettings["EmailBody"]
                    };
                    CommonService.SendMail(targetPath, emailModel);

                    logFile.WriteLine("Report is sent");
                }

                #endregion
            }
            catch (Exception ex)
            {
                logFile.Write("EXCEPTION {0}", ex.Message);
            }
            finally
            {
                logFile.WriteLine("===== END {0} =====", DateTime.Now);
                logFile.Close();
            }
        }

        private static Columns GetFormatColumns()
        {
            var xlCols = new Columns();
            var xlCol = new Column { Width = 70, BestFit = true, CustomWidth = true, Min = 1, Max = 1 };
            xlCols.Append(xlCol);

            xlCol = new Column { Width = 48, BestFit = true, CustomWidth = true, Min = 2, Max = 2 };
            xlCols.Append(xlCol);

            xlCol = new Column { Width = 25, BestFit = true, CustomWidth = true, Min = 3, Max = 3 };
            xlCols.Append(xlCol);

            xlCol = new Column { Width = 25, BestFit = true, CustomWidth = true, Min = 4, Max = 4 };
            xlCols.Append(xlCol);

            xlCol = new Column { Width = 50, BestFit = true, CustomWidth = true, Min = 5, Max = 5 };
            xlCols.Append(xlCol);

            xlCol = new Column { Width = 32, BestFit = true, CustomWidth = true, Min = 6, Max = 6 };
            xlCols.Append(xlCol);

            xlCol = new Column { Width = 32, BestFit = true, CustomWidth = true, Min = 7, Max = 7 };
            xlCols.Append(xlCol);

            xlCol = new Column { Width = 32, BestFit = true, CustomWidth = true, Min = 8, Max = 8 };
            xlCols.Append(xlCol);

            xlCol = new Column { Width = 32, BestFit = true, CustomWidth = true, Min = 9, Max = 9 };
            xlCols.Append(xlCol);

            return xlCols;
        }
    }
}
