﻿
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace FuchsReporting
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class CommonService
    {
        public static void SendMail(string targetPath, EmailModel emailModel)
        {
            if (emailModel == null)
                return;

            var mail = new MailMessage
            {
                Subject = emailModel.Subject,
                Body = emailModel.Body,
                IsBodyHtml = true,
                From = new MailAddress(emailModel.From, emailModel.FromName)
            };

            var toList = ParseEmailList(emailModel.To);
            foreach (var to in toList)
            {
                mail.To.Add(to);
            }

            var ccList = ParseEmailList(emailModel.CC);
            foreach (var cc in ccList)
            {
                mail.CC.Add(cc);
            }

            var bccList = ParseEmailList(emailModel.BCC);
            foreach (var bcc in bccList)
            {
                mail.Bcc.Add(bcc);
            }

            var attachment = new Attachment(targetPath);
            mail.Attachments.Add(attachment);

            var smtpClient = new SmtpClient { Port = 25, Host = "localhost", UseDefaultCredentials = true };
            smtpClient.Send(mail);
        }

        private static IEnumerable<string> ParseEmailList(string input)
        {
            input = FormatEmailList(input);

            if (string.IsNullOrEmpty(input)) return new List<string>();

            var emailList = input.Split(';').Select(email => email.Trim()).ToList();

            return emailList;
        }

        private static string FormatEmailList(string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return string.Empty;

            // Replace separate character
            input = input.Replace(',', ';');

            return input;
        }
    }

    
}
