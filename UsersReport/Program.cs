﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using ReportLib;

namespace UsersReport
{
    class Program
    {
        static void Main(string[] args)
        {
            var numberOfCreatedUsers = 0;
            var numberOfActiveUsers = 0;
            var numberOfDeactiveUsers = 0;
            var activeAccounts = new DataTable();
            var deactivatedAccounts = new DataTable();

            var logFile = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log.txt"), true);
            try
            {
                logFile.WriteLine();
                logFile.WriteLine("===== START {0} =====", DateTime.Now);

                // Get Data
                #region Get Data

                var connectionString = ConfigurationManager.ConnectionStrings["WebEd9_Fuchs"].ConnectionString;
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    logFile.WriteLine("Connected to Database");

                    var command = new SqlCommand("SELECT COUNT(*) FROM Users WHERE Created > DATEADD(WEEK, -1, GETUTCDATE()) AND RecordDeleted = 0", conn);
                    var result = command.ExecuteScalar();
                    if (result != null)
                    {
                        numberOfCreatedUsers = Convert.ToInt32(result);
                    }

                    command = new SqlCommand("SELECT COUNT(*) FROM Users WHERE Status = 1 AND RecordDeleted = 0", conn);
                    result = command.ExecuteScalar();
                    if (result != null)
                    {
                        numberOfActiveUsers = Convert.ToInt32(result);
                    }

                    command = new SqlCommand("SELECT COUNT(*) FROM Users WHERE Status = 2 AND RecordDeleted = 0", conn);
                    result = command.ExecuteScalar();
                    if (result != null)
                    {
                        numberOfDeactiveUsers = Convert.ToInt32(result);
                    }

                    command = new SqlCommand("SELECT Users.FirstName + ' ' + Users.LastName As Name, Users.Email ,Contacts.Company, Users.AccountExpiresDate FROM Users LEFT JOIN Contacts ON Users.Id = Contacts.UserId WHERE Users.Status = 1 AND Users.RecordDeleted = 0", conn);
                    activeAccounts.Load(command.ExecuteReader());
                    activeAccounts.TableName = "Active Users";

                    command = new SqlCommand("SELECT Users.FirstName + ' ' + Users.LastName As Name, Users.Email ,Contacts.Company, Users.AccountExpiresDate FROM Users LEFT JOIN Contacts ON Users.Id = Contacts.UserId WHERE Users.Status = 2 AND Users.RecordDeleted = 0", conn);
                    deactivatedAccounts.Load(command.ExecuteReader());
                    deactivatedAccounts.TableName = "Deactivated Users";

                    conn.Close();
                    conn.Dispose();
                    logFile.WriteLine("Close connection to Database");
                }

                #endregion

                // Open and write excel file
                #region Open and write excel file

                const string fileName = "FuchsWeeklyReport.xlsx";
                var sourcePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
                var targetFileName = string.Format("{0}_{1}.xlsx", Path.GetFileNameWithoutExtension(sourcePath),
                    DateTime.Now.ToString("ddMMMyyyy"));
                var reportDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Reports");
                string targetPath = Path.Combine(reportDirectory, targetFileName);

                if (!Directory.Exists(reportDirectory))
                {
                    Directory.CreateDirectory(reportDirectory);
                }

                if (File.Exists(sourcePath))
                {
                    var dateFormat = ConfigurationManager.AppSettings["DateFormat"];

                    File.Copy(sourcePath, targetPath, true);

                    var columnActiveAccountName = string.Empty;
                    var columnActiveAccountEmail = string.Empty;
                    var columnActiveAccountCompany = string.Empty;
                    var columnActiveAccountDeactivationDate = string.Empty;
                    UInt32Value styleIndexActiveAccountName = null;
                    UInt32Value styleIndexActiveAccountCompany = null;
                    UInt32Value styleIndexActiveAccountEmail = null;
                    UInt32Value styleIndexActiveAccountDeactivationDate = null;

                    var columnDeactivatedAccountName = string.Empty;
                    var columnDeactivatedAccountEmail = string.Empty;
                    var columnDeactivatedAccountCompany = string.Empty;
                    var columnDeactivatedAccountDeactivationDate = string.Empty;
                    UInt32Value styleIndexDeactivatedAccountName = null;
                    UInt32Value styleIndexDeactivatedAccountCompany = null;
                    UInt32Value styleIndexDeactivatedAccountEmail = null;
                    UInt32Value styleIndexDeactivatedAccountDeactivationDate = null;

                    using (var document = SpreadsheetDocument.Open(targetPath, true))
                    {
                        var sharedStringTable = document.WorkbookPart.SharedStringTablePart.SharedStringTable;
                        var workbookPart = document.WorkbookPart;
                        var sheets = workbookPart.Workbook.Descendants<Sheet>().ToList();
                        foreach (var sheet in sheets)
                        {
                            var rowIndexActiveAccount = 0;
                            var rowIndexDeactivatedAccount = 0;
                            var worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheet.Id);
                            var worksheet = worksheetPart.Worksheet;
                            var sheetData = worksheet.GetFirstChild<SheetData>();
                            var rows = worksheetPart.Worksheet.Descendants<Row>().ToList();

                            foreach (var row in rows)
                            {
                                foreach (var cell in row.Elements<Cell>())
                                {
                                    if (!string.IsNullOrEmpty(cell.InnerText))
                                    {
                                        var text = sharedStringTable.ElementAt(Int32.Parse(cell.InnerText)).InnerText;
                                        if (text.Contains(@"{TotalCreatedAccounts}"))
                                        {
                                            text = text.Replace(@"{TotalCreatedAccounts}", numberOfCreatedUsers.ToString());
                                            cell.CellValue = new CellValue(text);
                                            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                        }
                                        if (text.Contains(@"{TotalActiveAccounts}"))
                                        {
                                            text = text.Replace(@"{TotalActiveAccounts}", numberOfActiveUsers.ToString());
                                            cell.CellValue = new CellValue(text);
                                            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                        }
                                        if (text.Contains(@"{TotalDeactivatedAccounts}"))
                                        {
                                            text = text.Replace(@"{TotalDeactivatedAccounts}", numberOfDeactiveUsers.ToString());
                                            cell.CellValue = new CellValue(text);
                                            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                        }
                                        if (text.Contains(@"{ActiveAccountName}"))
                                        {
                                            Int32.TryParse(row.RowIndex, out rowIndexActiveAccount);
                                            cell.CellValue = new CellValue(activeAccounts.Rows[0][0].ToString());
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                            styleIndexActiveAccountName = cell.StyleIndex;

                                            var items = Regex.Split(cell.CellReference, @"\d+");
                                            if (items.Any())
                                            {
                                                columnActiveAccountName = items[0];
                                            }
                                        }
                                        if (text.Contains(@"{ActiveAccountEmail}"))
                                        {
                                            Int32.TryParse(row.RowIndex, out rowIndexActiveAccount);
                                            cell.CellValue = new CellValue(activeAccounts.Rows[0][1].ToString());
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                            styleIndexActiveAccountEmail = cell.StyleIndex;

                                            var items = Regex.Split(cell.CellReference, @"\d+");
                                            if (items.Any())
                                            {
                                                columnActiveAccountEmail = items[0];
                                            }
                                        }
                                        if (text.Contains(@"{ActiveAccountCompany}"))
                                        {
                                            Int32.TryParse(row.RowIndex, out rowIndexActiveAccount);
                                            cell.CellValue = new CellValue(activeAccounts.Rows[0][2].ToString());
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                            styleIndexActiveAccountCompany = cell.StyleIndex;

                                            var items = Regex.Split(cell.CellReference, @"\d+");
                                            if (items.Any())
                                            {
                                                columnActiveAccountCompany = items[0];
                                            }
                                        }
                                        if (text.Contains(@"{ActiveAccountDeactivationDate}"))
                                        {
                                            Int32.TryParse(row.RowIndex, out rowIndexActiveAccount);
                                            var cellValue = string.IsNullOrEmpty(activeAccounts.Rows[0][3].ToString())
                                                ? string.Empty
                                                : Convert.ToDateTime(activeAccounts.Rows[0][3].ToString())
                                                    .ToString(dateFormat);
                                            cell.CellValue = new CellValue(cellValue);
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                            styleIndexActiveAccountDeactivationDate = cell.StyleIndex;

                                            var items = Regex.Split(cell.CellReference, @"\d+");
                                            if (items.Any())
                                            {
                                                columnActiveAccountDeactivationDate = items[0];
                                            }
                                        }

                                        if (text.Contains(@"{DeactivatedAccountName}"))
                                        {
                                            Int32.TryParse(row.RowIndex, out rowIndexDeactivatedAccount);
                                            cell.CellValue = new CellValue(deactivatedAccounts.Rows[0][0].ToString());
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                            styleIndexDeactivatedAccountName = cell.StyleIndex;

                                            var items = Regex.Split(cell.CellReference, @"\d+");
                                            if (items.Any())
                                            {
                                                columnDeactivatedAccountName = items[0];
                                            }
                                        }
                                        if (text.Contains(@"{DeactivatedAccountEmail}"))
                                        {
                                            Int32.TryParse(row.RowIndex, out rowIndexDeactivatedAccount);
                                            cell.CellValue = new CellValue(deactivatedAccounts.Rows[0][1].ToString());
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                            styleIndexDeactivatedAccountEmail = cell.StyleIndex;

                                            var items = Regex.Split(cell.CellReference, @"\d+");
                                            if (items.Any())
                                            {
                                                columnDeactivatedAccountEmail = items[0];
                                            }
                                        }
                                        if (text.Contains(@"{DeactivatedAccountCompany}"))
                                        {
                                            Int32.TryParse(row.RowIndex, out rowIndexDeactivatedAccount);
                                            cell.CellValue = new CellValue(deactivatedAccounts.Rows[0][2].ToString());
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                            styleIndexDeactivatedAccountCompany = cell.StyleIndex;

                                            var items = Regex.Split(cell.CellReference, @"\d+");
                                            if (items.Any())
                                            {
                                                columnDeactivatedAccountCompany = items[0];
                                            }
                                        }
                                        if (text.Contains(@"{DeactivatedAccountDeactivationDate}"))
                                        {
                                            Int32.TryParse(row.RowIndex, out rowIndexDeactivatedAccount);
                                            var cellValue = string.IsNullOrEmpty(deactivatedAccounts.Rows[0][3].ToString())
                                                ? string.Empty
                                                : Convert.ToDateTime(deactivatedAccounts.Rows[0][3].ToString())
                                                    .ToString(dateFormat);
                                            cell.CellValue = new CellValue(cellValue);
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                            styleIndexDeactivatedAccountDeactivationDate = cell.StyleIndex;

                                            var items = Regex.Split(cell.CellReference, @"\d+");
                                            if (items.Any())
                                            {
                                                columnDeactivatedAccountDeactivationDate = items[0];
                                            }
                                        }
                                    }
                                }
                            }

                            if (rowIndexActiveAccount > 0)
                            {
                                for (var index = 1; index < activeAccounts.Rows.Count; index++)
                                {
                                    rowIndexActiveAccount += 1;

                                    var newRow = new Row();

                                    if (!string.IsNullOrEmpty(columnActiveAccountName))
                                    {
                                        var cell = new Cell()
                                        {
                                            CellReference = string.Format("{0}{1}", columnActiveAccountName, rowIndexActiveAccount),
                                            DataType = CellValues.String,
                                            CellValue = new CellValue(activeAccounts.Rows[index][0].ToString()),
                                            StyleIndex = styleIndexActiveAccountName
                                        };
                                        newRow.AppendChild(cell);
                                    }
                                    if (!string.IsNullOrEmpty(columnActiveAccountEmail))
                                    {
                                        var cell = new Cell()
                                        {
                                            CellReference = string.Format("{0}{1}", columnActiveAccountEmail, rowIndexActiveAccount),
                                            DataType = CellValues.String,
                                            CellValue = new CellValue(activeAccounts.Rows[index][1].ToString()),
                                            StyleIndex = styleIndexActiveAccountEmail
                                        };
                                        newRow.AppendChild(cell);
                                    }
                                    if (!string.IsNullOrEmpty(columnActiveAccountCompany))
                                    {
                                        var cell = new Cell()
                                        {
                                            CellReference = string.Format("{0}{1}", columnActiveAccountCompany, rowIndexActiveAccount),
                                            DataType = CellValues.String,
                                            CellValue = new CellValue(activeAccounts.Rows[index][2].ToString()),
                                            StyleIndex = styleIndexActiveAccountCompany
                                        };
                                        newRow.AppendChild(cell);
                                    }
                                    if (!string.IsNullOrEmpty(columnActiveAccountDeactivationDate))
                                    {
                                        var cellValue = string.IsNullOrEmpty(activeAccounts.Rows[index][3].ToString())
                                                ? string.Empty
                                                : Convert.ToDateTime(activeAccounts.Rows[index][3].ToString())
                                                    .ToString(dateFormat);
                                        var cell = new Cell()
                                        {
                                            CellReference = string.Format("{0}{1}", columnActiveAccountDeactivationDate, rowIndexActiveAccount),
                                            DataType = CellValues.String,
                                            CellValue = new CellValue(cellValue),
                                            StyleIndex = styleIndexActiveAccountDeactivationDate
                                        };
                                        newRow.AppendChild(cell);
                                    }

                                    sheetData.AppendChild(newRow);
                                }
                            }

                            if (rowIndexDeactivatedAccount > 0)
                            {
                                for (var index = 1; index < deactivatedAccounts.Rows.Count; index++)
                                {
                                    rowIndexDeactivatedAccount += 1;

                                    var newRow = new Row();

                                    if (!string.IsNullOrEmpty(columnDeactivatedAccountName))
                                    {
                                        var cell = new Cell()
                                        {
                                            CellReference = string.Format("{0}{1}", columnDeactivatedAccountName, rowIndexDeactivatedAccount),
                                            DataType = CellValues.String,
                                            CellValue = new CellValue(deactivatedAccounts.Rows[index][0].ToString()),
                                            StyleIndex = styleIndexDeactivatedAccountName
                                        };
                                        newRow.AppendChild(cell);
                                    }
                                    if (!string.IsNullOrEmpty(columnDeactivatedAccountEmail))
                                    {
                                        var cell = new Cell()
                                        {
                                            CellReference = string.Format("{0}{1}", columnDeactivatedAccountEmail, rowIndexDeactivatedAccount),
                                            DataType = CellValues.String,
                                            CellValue = new CellValue(deactivatedAccounts.Rows[index][1].ToString()),
                                            StyleIndex = styleIndexDeactivatedAccountEmail
                                        };
                                        newRow.AppendChild(cell);
                                    }
                                    if (!string.IsNullOrEmpty(columnDeactivatedAccountCompany))
                                    {
                                        var cell = new Cell()
                                        {
                                            CellReference = string.Format("{0}{1}", columnDeactivatedAccountCompany, rowIndexDeactivatedAccount),
                                            DataType = CellValues.String,
                                            CellValue = new CellValue(deactivatedAccounts.Rows[index][2].ToString()),
                                            StyleIndex = styleIndexDeactivatedAccountCompany
                                        };
                                        newRow.AppendChild(cell);
                                    }
                                    if (!string.IsNullOrEmpty(columnDeactivatedAccountDeactivationDate))
                                    {
                                        var cellValue = string.IsNullOrEmpty(deactivatedAccounts.Rows[index][3].ToString())
                                               ? string.Empty
                                               : Convert.ToDateTime(deactivatedAccounts.Rows[index][3].ToString())
                                                   .ToString(dateFormat);
                                        var cell = new Cell()
                                        {
                                            CellReference = string.Format("{0}{1}", columnDeactivatedAccountDeactivationDate, rowIndexDeactivatedAccount),
                                            DataType = CellValues.String,
                                            CellValue = new CellValue(cellValue),
                                            StyleIndex = styleIndexDeactivatedAccountDeactivationDate
                                        };
                                        newRow.AppendChild(cell);
                                    }

                                    sheetData.AppendChild(newRow);
                                }
                            }
                            worksheetPart.Worksheet.Save();
                        }

                        document.Close();
                    }
                    
                    // Send mail
                    var emailModel = new EmailModel
                    {
                        From = ConfigurationManager.AppSettings["EmailFrom"],
                        FromName = ConfigurationManager.AppSettings["EmailFromName"],
                        To = ConfigurationManager.AppSettings["EmailTo"],
                        CC = ConfigurationManager.AppSettings["EmailCC"],
                        BCC = ConfigurationManager.AppSettings["EmailBCC"],
                        Subject = ConfigurationManager.AppSettings["EmailSubject"],
                        Body = ConfigurationManager.AppSettings["EmailBody"]
                    };
                    CommonService.SendMail(targetPath, emailModel);
                    logFile.WriteLine("Report is sent");
                }

                #endregion

            }
            catch (Exception ex)
            {
                logFile.Write("EXCEPTION {0}", ex.Message);
            }
            finally
            {
                logFile.WriteLine("===== END {0} =====", DateTime.Now);
                logFile.Close();
            }
        }
    }
}
